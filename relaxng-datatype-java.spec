Name:           relaxng-datatype-java
Version:        2011.1
Release:        4%{?dist}
Summary:        The relaxng datatype library for Java
# License file is not present in the source repository, the file was retrieved
# from SourceForge where the previous version is hosted
# https://sourceforge.net/projects/relaxng/files/datatype%20%28java%29/Ver.1.0/relaxngDatatype-1.0.zip/download
License:        BSD-3-Clause
URL:            https://relaxng.org/
BuildArch:      noarch
ExclusiveArch:  %{java_arches} noarch

Source0:        https://github.com/java-schema-utilities/%{name}/archive/refs/tags/relaxngDatatype-%{version}.tar.gz
Source1:        copying.txt

BuildRequires:  maven-local

%description
Interface between RELAX NG validators and datatype libraries.

%package        javadoc
Summary:        API documentation for %{name}

%description    javadoc
This package provides %{summary}.

%prep
%setup -q -n %{name}-relaxngDatatype-%{version}
cp %{SOURCE1} .

%pom_remove_parent

%pom_xpath_remove 'pom:build/pom:extensions'

%mvn_alias com.github.relaxng:relaxngDatatype relaxngDatatype:relaxngDatatype

%build
%mvn_build -- -Dmaven.compiler.release=8

%install
%mvn_install

%files -f .mfiles
%license copying.txt

%files javadoc -f .mfiles-javadoc
%license copying.txt

%changelog
* Mon Jun 24 2024 Troy Dawson <tdawson@redhat.com> - 2011.1-4
- Bump release for June 2024 mass rebuild

* Fri Jan 26 2024 Fedora Release Engineering <releng@fedoraproject.org> - 2011.1-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_40_Mass_Rebuild

* Mon Jan 22 2024 Fedora Release Engineering <releng@fedoraproject.org> - 2011.1-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_40_Mass_Rebuild

* Mon Aug 07 2023 Marian Koncek <mkoncek@redhat.com> - 2011.1-1
- Initial build
